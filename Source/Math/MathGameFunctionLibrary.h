// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MathGameFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class MATH_API UMathGameFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "CalculateFromString", Keywords = "Calculate Math Expression From String"), Category = "MathGameLibrary")
	static float CalculateFromString(TArray<FString> Expression);
	
};
