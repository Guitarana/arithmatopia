// Fill out your copyright notice in the Description page of Project Settings.


#include "MathGameFunctionLibrary.h"
#include <stdlib.h>

UMathGameFunctionLibrary::UMathGameFunctionLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

float UMathGameFunctionLibrary::CalculateFromString(TArray<FString> Expression)
{
	bool IsMulDiv = false;
	int MulDivIndex = 0;
	float AcumulatedValue = 0.0f;

	for (int i = 0; i < Expression.Num(); i++)
	{
		if ((Expression[i] == "*") || (Expression[i] == "/"))
		{
			MulDivIndex = i;
			IsMulDiv = true;
		}

	}

	if (IsMulDiv)
	{
		if (Expression[MulDivIndex] == "*")
			AcumulatedValue = (FCString::Atof(*Expression[MulDivIndex - 1])) * (FCString::Atof(*Expression[MulDivIndex + 1]));
		if (Expression[MulDivIndex] == "/")
			AcumulatedValue = (FCString::Atof(*Expression[MulDivIndex - 1])) / (FCString::Atof(*Expression[MulDivIndex + 1]));

		if (Expression.Num() > 3)
		{
			if (MulDivIndex == 1)
			{
				if (Expression[3] == "*")
					AcumulatedValue = AcumulatedValue * (FCString::Atof(*Expression[4]));
				if (Expression[3] == "/")
					AcumulatedValue = AcumulatedValue / (FCString::Atof(*Expression[4]));
				if (Expression[3] == "+")
					AcumulatedValue = AcumulatedValue + (FCString::Atof(*Expression[4]));
				if (Expression[3] == "-")
					AcumulatedValue = AcumulatedValue - (FCString::Atof(*Expression[4]));
			}
			else if (MulDivIndex == 3)
			{
				if (Expression[1] == "*")
					AcumulatedValue = (FCString::Atof(*Expression[0])) * AcumulatedValue;
				if (Expression[1] == "/")
					AcumulatedValue = (FCString::Atof(*Expression[0])) / AcumulatedValue;
				if (Expression[1] == "+")
					AcumulatedValue = (FCString::Atof(*Expression[0])) + AcumulatedValue;
				if (Expression[1] == "-")
					AcumulatedValue = (FCString::Atof(*Expression[0])) - AcumulatedValue;
			}
		}

	}
	else
	{
		if (Expression[1] == "+")
			AcumulatedValue = (FCString::Atof(*Expression[0])) + (FCString::Atof(*Expression[2]));
		if (Expression[1] == "-")
			AcumulatedValue = (FCString::Atof(*Expression[0])) - (FCString::Atof(*Expression[2]));
	
		if (Expression.Num() > 3)
		{
			if (Expression[3] == "+")
				AcumulatedValue = AcumulatedValue + (FCString::Atof(*Expression[4]));
			if (Expression[3] == "-")
				AcumulatedValue = AcumulatedValue - (FCString::Atof(*Expression[4]));
		}
	}

	return AcumulatedValue;
}